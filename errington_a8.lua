--Thomas Errington
A--ssignment 8
--CS3060 
--19 Nov 17

-- Problem One
function ends_in_3(num)
	return num	% 10 == 3
end
-- A few test cases
print("A few test cases should result in F,T,T,F,F")
print(ends_in_3(10))
print(ends_in_3(3))
print(ends_in_3(23465432353423))
print(ends_in_3(123091843092578093275893479853478902734980173247234893))
print(ends_in_3(2345.323))

-- Problem Two--------------------------------------------------------
function is_prime(num)
	if num < 2 then
		return false
	end

	if num % 2 == 0 then
		return false
	end

	for i = 2, num - 1  do
		if num % i == 0 then
			return false
		end
	end
	return num
end

print("")
print ("Test for  problem two are 1,3,7,22,23,509 -> F,T,T,F,T,T")
print(is_prime(1))
print(is_prime(3))
print(is_prime(7))
print(is_prime(22))
print(is_prime(23))
print(is_prime(509))

-- Problem Three -----------------------------------------------------
function primes_of_3(num)
                                                          
	for i = 1, num - 1 do
		if is_prime(i) then
			if ends_in_3(i) then
				print (i)
			end
		end
	end
end

print("")
print("Test for problem three primes up to 100")
primes_of_3(100)

-- Problem Four------------------------------------------------------

print("")
print("Random list filled with 100 values")

-- Gererate a random list of 100 values from 1 - 100
myList = {}

for i = 1, 100 do
	myList[i] = math.random(1,100)
end

-- print random list
for i = 1, 100 do
	print(myList[i])
end

print("")
print("Sorted print")

function problemFour(myList)
	
	-- create new list to edit and set to the random list passed into the function
	sortedList = {}
	sortedList = myList

	-- built in sort function
	table.sort(sortedList)

	-- print sorted list
	for i = 1, 100 do
		print(sortedList[i])
	end

	print("")
	print("reversed")
	-- create new list to edit for reverse sort
	reverseSort = {}

	-- gets the size of the list
	local  sizeOf = #sortedList

	-- flips the values based on the count anf size
	for i = sizeOf, 1, -1 do
		reverseSort[sizeOf - i + 1] = sortedList[i]
	end
	
	-- reverse sort print
	for i = 1, 100 do
		print(reverseSort[i])
	end

	print("")
	print("odds")

	-- create new list to edit for odds
	oddList = {}
	count = 1

	-- use modulo to check for odd index and assign it to the new list
	for i = 1, 100 do
		if count % 2 == 1 then
			oddList[i] = sortedList[count]
			
		end
		count = count + 1
	end

	-- print the odd list
	-- for some reason it also fills the list with nil in the even spots
	-- this is an unwanted sideeffect but it helped check the values when i 
	-- compared them in excel so its a good bug ;-)
	for i = 1, 50 do
		print(oddList[i])
	end

end

problemFour(myList)

-- Problem Five -----------------------------------------------
print("")
print ("Selection Sort")

-- standard selection sort method here with flipping the values depending on value
function problemFive(myList2)
	for i = 1, #myList2 - 1 do
		first = i
		for j = i + 1, #myList2 do
			if myList2[j] < myList2[first] then
				first = j
			end
		end
		myList2[i], myList2[first] = myList2[first], myList2[i]
	end
end

-- create and filling of a new random list to edit
myList2 = {}

for i = 1, 100 do
	myList2[i] = math.random(1,100)
end

problemFive(myList2)

-- print the sorted list
for i = 1, 100 do
	print(myList2[i])
end

--Problem Six------------------------------------------------

print("")
print("Test cases for six")

-- add function to add two values
function add(first, second)
	return	first + second
end

-- this is the foldl function
function problemSix(max, init, f)
	holder = 0

	-- for loops passes the two values to the add functions
	for i = 1, max do
		holder = f(holder, i)
	end
	return holder
end

-- print calls
print(problemSix(10,6,add))
print(problemSix(1,2,add))

--Problem Seven--------------------------------------

print("")
print("Test cases for seven")
--check for factorial
function factorial(number)
	local x = 1
	for i = 2, number do
		x = x * i
	end
	return x
end

-- print calls
print(factorial(10))
print(factorial(2))
